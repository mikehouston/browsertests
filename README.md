# GopherJS browser tests #

Run compiled Go tests in the browser using GopherJS

## Usage

Import the browsertests package in one of your tests to ensure the browser interface is initialised:
~~~go
import _ "bitbucket.org/mikehouston/browsertests"
~~~

To compile your tests, use GopherJS:
~~~sh
$ gopherjs test -c
~~~

To run the tests you will need an HTML file to load the JavaScript:
~~~html
<html>
    <body>
        <p>To recompile tests use <code>gopherjs test -c</code></p>
        <script src="browsertests_test.js"></script>
    </body>
</html>
~~~

Then run the tests by opening the HTML file in a browser. The easiest way to do this is
to run the `gopherjs serve` command and navigate to the package you want to test.

![Screenshot_29_08_2016__16_00.jpg](https://bitbucket.org/repo/Mo7ran/images/1100109929-Screenshot_29_08_2016__16_00.jpg)

## Installation

~~~
$ go get -u bitbucket.org/mikehouston/browsertests
~~~