// +build js

package browsertests

import (
	"net/url"
	"os"
	"path"

	"github.com/gopherjs/gopherjs/js"
)

// See https://github.com/shurcooL/eX0/blob/17918f464a7c40e276bed59bb2765dc5186409e3/eX0-go/main_js.go#L26-L48
func queryToArgs() []string {
	u, err := url.Parse(js.Global.Get("location").Get("href").String())
	if err != nil {
		panic(err)
	}
	args := []string{path.Base(u.Path)} // First element is the process name.
	for k, vs := range u.Query() {
		for _, v := range vs {
			args = append(args, k)
			if v != "" {
				args = append(args, v)
			}
		}
	}
	return args
}

func init() {
	// Don't run in Node
	if process := js.Global.Get("process"); process != js.Undefined {
		return
	}

	// Update os.Args
	os.Args = queryToArgs()
	if len(os.Args) == 1 {
		os.Args = append(os.Args, "-test.short") // Default mode when no parameters are provided.
	}

	// Check which options are currently enabled
	v := ""
	short := ""
	bench := ""

	for _, arg := range os.Args {
		if arg == "-test.v" {
			v = "checked"
		}
		if arg == "-test.short" {
			short = "checked"
		}
		if arg == "-test.bench" {
			bench = "checked"
		}
	}

	document := js.Global.Get("document")
	document.Call("write", `<form>`+
		`<label><input type="checkbox" name="-test.v" value="" `+v+`>Verbose</label> `+
		`<label><input type="checkbox" name="-test.short" value="" `+short+`>Short</label> `+
		`<label><input type="checkbox" name="-test.bench" value="." `+bench+`>Benchmarks</label> `+
		`<input type="hidden" name="test">`+
		`<input type="submit" value="Run tests">`+
		`</form>`,
	)
}
