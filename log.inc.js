var last = Date.now();
var clog = console.log;

function documentlog() {
    var now = Date.now();
    var body = document.getElementsByTagName("body")[0]
    var row = '<pre style="margin: 5px 0 0 5em; border-left: 1px solid #CCC; padding-left: 5px;">';
    
    var delta = (now - last);
    if (delta > 0) {
        row += '<span style="position:absolute; left:5px; width: 4.5em; text-align: right; color: red;">+' + delta + 'ms</span>';
    }
    last = now;
    
    for (var i = 0; i < arguments.length; i++) {
        if (i > 0) {
            row += " ";
        }
        if (typeof arguments[i] === "string") {
            row += arguments[i];
        } else if (arguments[i].$array) {
            row += JSON.stringify(arguments[i].$array);
        } else if (arguments[i].$val && arguments[i].Object) {
            row += JSON.stringify(arguments[i].Object);
        } else {
            row += arguments[i];
        }
    }
    row += "&nbsp;</pre>";
    body.innerHTML += row;
    clog.apply(null, arguments);
}

if (typeof window !== 'undefined') {
    console.log = documentlog;
}
