package browsertests

import (
	"testing"
	// Required in other packages
	// _ bitbucket.org/mikehouston/browsertests
)

func TestPass(t *testing.T) {
	t.Log("Test should pass")
}
func TestFail(t *testing.T) {
	t.Log("Test should fail")
	t.Fatal()
}
