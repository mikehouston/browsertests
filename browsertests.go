// Package browsertests provides a convenient interface when running Go tests
// in the browser via `GopherJS test -c`
package browsertests
